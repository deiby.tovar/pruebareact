import React from 'react';
import Home from './pages/home/Home';


import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.js'
import 'popper.js'
import 'bootstrap/dist/js/bootstrap.js';


function App() {
  return (
    <div>
      <Home />
    </div>
  );
}

export default App;
