import React, { Component } from 'react'
import MovieService from '../../services/MovieService';
import CardPelicula from '../../componentes/CardPelicula';

export default class Home extends Component {

    state = {
        movies: []
    }

    componentDidMount = () => {
        MovieService.all().then(resp => {
            console.log(resp);
            this.setState({
                movies: resp.data.results
            })
        }).catch(erro => {

        })
    }



    render() {
        return (
            <div>
                <header>
                    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                        <a className="navbar-brand" href="/">Peliculas DB</a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item active">
                                    <a className="nav-link" href="/">Incio <span className="sr-only">(current)</span></a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header>
                <div className="row mt-3">
                    <div className="container">
                        <div className="row">
                            {
                                this.state.movies.map(pelicula => <CardPelicula pelicula={pelicula} />)
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


const styles = {};

styles.prueba = {
    color: 'red'
}

styles.otra = {
    ...styles.prueba,
    fontSize: "100px"
}