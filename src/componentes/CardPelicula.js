import React from 'react';
import { BASE_IMG } from '../globals';

const CardPelicula = (props) => {
    const { pelicula } = props;
    return (
        <div className="col-md-4 col-sm-6 col-12 mb-3">
        <div className="card" key={pelicula.id}>
            <img src={BASE_IMG + pelicula.poster_path} className="card-img-top" alt="..." />
            <div className="card-body">
                <h5 className="card-title">{pelicula.title}</h5>
                <p className="card-text">{pelicula.overview}</p>
                <a href="#" className="btn btn-primary">Go somewhere</a>
            </div>
        </div>
        </div>
    );
};

export default CardPelicula;