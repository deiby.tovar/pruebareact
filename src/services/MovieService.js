import axios from 'axios';
import { BASE_URL, BUSCAR, DETALLE, API_KEY } from '../globals';

export default {
    all(query='marvel') {
        const complemento = `${BASE_URL}${BUSCAR}query=${query}&language=es-ES`;
        return axios.get(complemento);
    },
    find(id) {
        return axios.get(`${BASE_URL}${DETALLE}${id}?${API_KEY}&language=es-ES`);
    },
};